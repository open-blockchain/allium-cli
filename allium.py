"""ALLIUM HASH CLI
Usage:
    allium.py <string>
    allium.py -h|--help
    allium.py -v|--version
Options:
    <string>  Optional string argument.
    -h --help  Show this screen.
    -v --version  Show version.
"""
from docopt import docopt
import sys
import allium_hash
import warnings

def print_allium(cliarg):
    arr = cliarg.encode()
    hash = ''
    hash = allium_hash.getPoWHash(arr)
    print(hash.hex())

if __name__ == '__main__':
    warnings.filterwarnings("ignore", category=DeprecationWarning)
    arguments = docopt(__doc__, version='DEMO 1.0')
    if arguments['<string>']:
        print_allium(arguments['<string>'].zfill(80))
    else:
        print(arguments)
